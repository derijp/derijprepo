package com.example.gebruiker.derijp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonReader;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity  {
    private EditText editText_login_username;
    TextView tv_steps;
    SensorManager sensorManager;
    HttpsURLConnection myConnection;
    URL url;
    String apiURL;
    InputStreamReader responseBodyReader;
    static SharedPreferences settings;
    static SharedPreferences.Editor editor;
    private Button button_login_login;
    public String loginCode;
    static  String loginId ;
    static String token;
    String naam;
    static String doel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        runOnUiThread(new Runnable() {
            public void run()
            {
                settings = getPreferences(MODE_PRIVATE);
                //Haal local variable login op
                String val = settings.getString("login", "0");
                loginId = settings.getString("loginId", "0");

                // 1 betekend ingelogd en ga naar activity 2
                if (val.equals("1")){
                    Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                    startActivity(intent); // startActivity allow you to move
                }
            }
        });

        //ingevoerde code zoeken
        editText_login_username = (EditText) findViewById(R.id.editText_login_username);

        //button vinden
        button_login_login = (Button) findViewById(R.id.button_login_login);

        //als er op knop gedrukt word
        button_login_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //pak ingevoerde code
                    loginCode = editText_login_username.getText().toString();

                    if (loginCode.equals("")){
                        loginCode = " CODE";
                    }


                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {

                            //plak code achter apiURL
                            maakConectie(loginCode);
                            try {
                                //maak connectie
                                myConnection = (HttpsURLConnection) url.openConnection();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (myConnection.getResponseCode() == 200) {
                                    //Doe api call
                                    InputStream responseBody = myConnection.getInputStream();

                                    //krijg response
                                    responseBodyReader = new InputStreamReader(responseBody, "UTF-8");
                                } else {
                                    // Error handling code goes here
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            //zet respone naar json
                            JsonReader jsonReader = new JsonReader(responseBodyReader);
                            try {
                                jsonReader.beginObject(); // Start processing the JSON object
                                while (jsonReader.hasNext()) { // Loop through all keys

                                    String key = jsonReader.nextName(); // Fetch the next key
                                    if (key.equals("token")) { // Check if desired key
                                        // Fetch the value as a String
                                        String value = jsonReader.nextString();

                                        //als in respone '1' staat login
                                        if (value.equals(loginCode)){
                                            token = value;
                                            //ga naar seond activity
                                            Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                                            startActivity(intent); // startActivity allow you to move

                                            //geef login geslaagd message
                                            loginmessage(naam);

                                            //Zet ingelogd in local storage
                                            settings = getPreferences(MODE_PRIVATE);
                                            editor = settings.edit();
                                            editor.putString("login", "1");
                                            editor.putString("loginId", "3");
                                            editor.commit();
                                        }
                                        else{
                                            loginfailmessage();
                                        }
                                        // Do something with the value
                                        // ...
                                        break; // Break out of the loop
                                    } else if(key.equals("firstname")){
                                        naam = jsonReader.nextString();
                                    }
                                    else {
                                        jsonReader.skipValue(); // Skip values of other keys
                                    }
                                }
                                jsonReader.close();

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            myConnection.disconnect();
                        }
                    });
                } catch (Exception ex) {
                }
            }
        });
    }



    static void loguit(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                editor = settings.edit();
                editor.putString("login", "0");
                editor.commit();
            }
        });
    }
    public  void loginmessage(final String voornaam){

        runOnUiThread(new Runnable() {
            public void run()
            {
                Context context = getApplicationContext();
                CharSequence text = "U bent ingelogd als: " + voornaam;
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }

    public  void loginfailmessage(){

        runOnUiThread(new Runnable() {
            public void run()
            {
                Context context = getApplicationContext();
                CharSequence text = "verkeerde login code";
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }

    public  void maakConectie(String parameter){
        try {
            url = new URL("https://api.fysio-de-rijp.nl/api/patientApp/" + parameter);
        } catch (IOException e) {
            e.printStackTrace();
            }
        }
    }