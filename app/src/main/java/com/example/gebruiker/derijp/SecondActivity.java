package com.example.gebruiker.derijp;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonReader;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class SecondActivity extends  AppCompatActivity implements SensorEventListener {
    int steps;
    TextView tv_steps;
    TextView tv_doel;
    TextView tv_doel2;
    SensorManager sensorManager;
    HttpsURLConnection myConnection;
    URL url;
    InputStreamReader responseBodyReader;
    Button button_login_loginout;
    CircularProgressBar circularProgressBar;
    int doel = -1;
    int progress = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tv_steps = findViewById(R.id.tv_steps);
        tv_doel = findViewById(R.id.tv_doel);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        button_login_loginout = findViewById(R.id.button_login_loginout);

        //Wanneer op logout word geklikt
        button_login_loginout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            MainActivity.loguit();
                            Intent intent = new Intent(SecondActivity.this, MainActivity.class);
                            startActivity(intent); // startActivity allow you to move
                            logoutMessage();
                        }
                    });
                } catch (Exception ex) {
                }
            }
        });

        setNotification();
        setProgress(1500, progress);
        apiCall();
        while (doel == -1) {
        }
        tv_doel.setText(String.valueOf(doel));

        hoogOp();
    }

    public void setProgress(int animationDuration, int progress) {
        circularProgressBar = findViewById(R.id.progress);
        circularProgressBar.setProgressWithAnimation(progress, animationDuration); // Default duration = 1500ms
    }

    public void logoutMessage() {
        runOnUiThread(new Runnable() {
            public void run() {
                Context context = getApplicationContext();
                CharSequence text = "U bent uitgelogd";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (countSensor != null) {
            sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_FASTEST);
        } else {
            Toast.makeText(this, "Sensor niet gevonden!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        tv_steps.setText(String.valueOf(Math.round(event.values[0])));
        steps = Math.round(event.values[0]);
        if (steps < doel) {
            progress = (int) (steps/(doel + .0 ) * 100);
            setProgress(1500, progress);
        }
        else{
            progress = 100;
            setProgress(1500, progress);
        }

        hoogOp();
    }

    public void hoogOp(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    url = new URL("https://api.fysio-de-rijp.nl/api/patientSteps/" +  MainActivity.token  + "/" + steps);
                    myConnection = (HttpsURLConnection) url.openConnection();

                    InputStream responseBody = myConnection.getInputStream();
                    responseBodyReader = new InputStreamReader(responseBody, "UTF-8");

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void apiCall() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    url = new URL("https://api.fysio-de-rijp.nl/api/patientApp/" + MainActivity.token);
                    myConnection = (HttpsURLConnection) url.openConnection();

                    if (myConnection.getResponseCode() == 200) {
                        InputStream responseBody = myConnection.getInputStream();
                        responseBodyReader = new InputStreamReader(responseBody, "UTF-8");
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                JsonReader jsonReader = new JsonReader(responseBodyReader);
                try {
                    jsonReader.beginObject(); // Start processing the JSON object
                    while (jsonReader.hasNext()) { // Loop through all keys

                        String key = jsonReader.nextName(); // Fetch the next key
                        if (key.equals("goal")) { // Check if desired key

                            // Fetch the value as a String
                            int value = jsonReader.nextInt();
                            //     String[] array = value.split(",");
                            doel = value;
                            //   tv_doel.setText(String.valueOf(doel));
                            break; // Break out of the loop
                        } else {
                            jsonReader.skipValue(); // Skip values of other keys
                        }
                    }
                    jsonReader.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                myConnection.disconnect();
            }
        });
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void setNotification(){
    }
}

